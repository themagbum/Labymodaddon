package net.freezegames.addon;

import net.labymod.api.events.TabListEvent;
import net.labymod.main.LabyMod;
import net.labymod.servermanager.ChatDisplayAction;
import net.labymod.servermanager.Server;
import net.labymod.settings.elements.SettingsElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.client.config.GuiConfigEntries;

import java.util.List;

public class FreezeGamesServer extends Server {
    public FreezeGamesServer() {
        super("FreezeGames", "FreezeGames.net", "Freezegames.de", "bunge1.freezegames.net");
        FreezeGamesAddon.plugin.getApi().registerServerSupport(FreezeGamesAddon.plugin, this);
    }

    @Override
    public void onJoin(ServerData serverData) {
        if(FreezeGamesAddon.plugin.enabled) {
            FreezeGamesAddon.plugin.isonfreezegames = true;
            LabyMod.getInstance().displayMessageInChat("§cDu bist auf FreezeGames gejoint! \n§eDas Addon ist nun aktiv! \n§aBei Support §b/support§a oder im discord §b/discord");
        }

    }

    @Override
    public ChatDisplayAction handleChatMessage(String clean, String formatted) throws Exception {
       if(clean.contains(" -> Dir] ") || formatted.contains("§r§e[§r§bDu §r§e-> ")) {
           return ChatDisplayAction.SWAP;
        }
       return ChatDisplayAction.NORMAL;
    }

    @Override
    public void handlePluginMessage(String s, PacketBuffer packetBuffer) throws Exception {

    }

    @Override
    public void handleTabInfoMessage(TabListEvent.Type type, String s, String s1) throws Exception {

    }

    @Override
    public void fillSubSettings(List<SettingsElement> list) {

    }
}
