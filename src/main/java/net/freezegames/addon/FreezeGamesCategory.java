package net.freezegames.addon;

import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.ModuleCategoryRegistry;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public class FreezeGamesCategory extends ModuleCategory {
    public FreezeGamesCategory() {
        super("FreezeGames", true, new ControlElement.IconData("freezegames/textures/icon.png"));
        ModuleCategoryRegistry.loadCategory(this);
    }
}
