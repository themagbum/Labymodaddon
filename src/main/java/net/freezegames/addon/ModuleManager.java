package net.freezegames.addon;

import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.ModuleCategoryRegistry;
import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public class ModuleManager extends SimpleModule {
    @Override
    public String getDisplayName() {
        return "BD-Points";
    }

    @Override
    public String getDisplayValue() {
        return FreezeGamesAddon.plugin.beacondefensepoint + " Points";
    }

    @Override
    public String getDefaultValue() {
        return String.valueOf(0);
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.BOOK);
    }

    @Override
    public void loadSettings() {}

    @Override
    public String getSettingName() {
        return "BD-Points";
    }

    @Override
    public String getDescription() {
        return "Hier Siehst du die Points in BeaconDefense";
    }

    @Override
    public int getSortingId() {
        return 0;
    }
    @Override
    public ModuleCategory getCategory() {
        return FreezeGamesAddon.plugin.freezeGamesCategory;
    }
}
