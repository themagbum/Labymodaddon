package net.freezegames.addon;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.labymod.api.LabyModAddon;
import net.labymod.api.events.MessageReceiveEvent;
import net.labymod.api.events.ServerMessageEvent;
import net.labymod.main.LabyMod;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;
import net.labymod.utils.ServerData;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.util.List;

public class FreezeGamesAddon extends LabyModAddon {
    public boolean enabled = true;
    public boolean isonfreezegames = false;
    public int beacondefensepoint = 0;
    public static FreezeGamesAddon plugin;
    public FreezeGamesCategory freezeGamesCategory;
    @Override
    public void onEnable() {
        plugin = this;
        System.out.println("Enable FreezeGamesAddon!");
        this.getApi().getEventManager().registerOnQuit(new Consumer<ServerData>() {
                                                           @Override
                                                           public void accept(ServerData serverData) {
                                                               if (!enabled) {
                                                                   return;
                                                               }
                                                               isonfreezegames = false;
                                                           }
                                                       }
        );
        this.getApi().getEventManager().register( new ServerMessageEvent() {

            public void onServerMessage( String messageKey, JsonElement serverMessage ) {
                if( messageKey.equals( "BeaconDefensePoints" ) && serverMessage.isJsonObject() ) {
                    JsonObject pointsObject = serverMessage.getAsJsonObject();

                    // Checking whether a points attribute is included
                    if( pointsObject.has( "points" ) ) {
                        // Getting the value of the points attribute
                        int newpoints = pointsObject.get( "points" ).getAsInt();

                        // Setting the points
                        beacondefensepoint =  newpoints;
                        getConfig().addProperty("BeaconDefensePoint", newpoints);
                        saveConfig();
                        // Printing a log message
                        System.out.println( "You have got " + newpoints + " points now!" );
                    }
                }
            }

        } );
        freezeGamesCategory = new FreezeGamesCategory();
        this.getApi().registerModule(new ModuleManager());
        new FreezeGamesServer();
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {
        this.enabled = getConfig().has("Enable") ? getConfig().get("Enable").getAsBoolean() : true;
        this.beacondefensepoint = getConfig().has("BeaconDefensePoint") ? getConfig().get("BeaconDefensePoint").getAsInt() : 0 ;
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
        final BooleanElement booleanElement = new BooleanElement("Enabled", new ControlElement.IconData(Material.EMERALD), new Consumer<Boolean>() {
            @Override
            public void accept(Boolean enabled) {
                FreezeGamesAddon.this.enabled = enabled;
                FreezeGamesAddon.this.getConfig().addProperty("Enable", enabled);
                FreezeGamesAddon.this.saveConfig();
            }
        }, this.enabled);
        list.add(booleanElement);
    }
}
